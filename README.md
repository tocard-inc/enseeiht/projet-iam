# README

Ce repository contient l'implémentation de deux réseaux de deep learning pour l'apprentissage semi-supervisé: simCLR et SGAN. Les deux réseaux ont été testés sur des jeux de données de classification d'images.

## simCLR

simCLR est une approche semi-supervisée pour l'apprentissage de représentations à partir d'un grand nombre d'images non étiquetées. Le modèle consiste en une chaîne de transformation qui transforme les entrées dans un espace d'apprentissage continu et en un classificateur qui prédit les labels des entrées transformées.

## SGAN

SGAN est un réseau GAN semi-supervisé qui utilise une générateur pour produire des images aléatoires et un discriminateur pour les classer en fonction des labels connus. Le but est de produire des images qui sont à la fois réalistes et informatives pour l'apprentissage de la classification.
Comment utiliser ce repository

## Comment utiliser ce repository

Les notebooks (colab) se situent dans le dossier `src`.


## Contacts

- Laurent Fainsin <laurent@fainsin.bzh>
- Damien Guillotin <damien.guillotin@etu.inp-n7.fr>
- Pierre-Eliot Jourdan <pierreeliot.jourdan@etu.inp-n7.fr>

## License MIT
